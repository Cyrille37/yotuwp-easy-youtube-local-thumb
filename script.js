jQuery(document).ready(function ($) {
    "use strict";
    var $yotuwp = $('.yotuwp');
    $('a.yotu-video',$yotuwp).each(function(idx,ele){
        var $el = $(ele);
        var videoId = $el.data('videoid');
        $el
            .prop('target', '_blank')
            .prop('href','https://www.youtube.com/watch?v='+videoId);

    });

    $('a.yotu-video .yotu-video-thumb-wrp',$yotuwp).append(
        $('<div class="yotu-link"><span>Voir sur Youtube</span></div>')
    );
});
