<?php

/**
 * yotuwp-easy-youtube-local-thumb
 *
 * @package           yotuwp-easy-youtube-local-thumb
 * @author            Cyrille37
 * @copyright         2023 Cyrille37
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       yotuwp-easy-youtube-local-thumb
 * Plugin URI:        https://framagit.org/Cyrille37/yotuwp-easy-youtube-local-thumb
 * Description:       Overload "YotuWP - YouTube Gallery" to avoid use of Youtube API and thumbnail access
 * Version:           1.0.0
 * Requires at least: 6.0
 * Requires PHP:      7.3
 * Author:            Cyrille37
 * Author URI:        https://framagit.org/Cyrille37
 * Text Domain:       plugin-slug
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Update URI:        https://framagit.org/Cyrille37/yotuwp-easy-youtube-local-thumb
 */

if (!defined('WPINC')) {
    die;
}

require_once(__DIR__ . '/libraries/action-scheduler/action-scheduler.php');

new YotuwpEasyYoutubeLocalThumb();

class YotuwpEasyYoutubeLocalThumb
{
    const PLUGIN_NAME = 'yotuwpeasyyoutubelocalthumb';
    const PLUGIN_VERSION = '1.0';

    const ASYNC_ACTION_DOWNLOAD_THUMBNAIL = self::PLUGIN_NAME . '_download-thumbnail';

    public $plugin_url;

    public $cache_path;
    public $cache_url;

    public function __construct()
    {
        $this->plugin_url = plugin_dir_url(__FILE__);
        $this->cache_path = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . self::PLUGIN_NAME;
        $this->cache_url = content_url('cache/' . self::PLUGIN_NAME);

        if (is_blog_admin()) {
            // No admin specific stuff
        } else {
            if (!$this->ignoreRequest()) {
                add_action('init', [$this, 'wp_init']);
            }
        }
        add_action(self::ASYNC_ACTION_DOWNLOAD_THUMBNAIL, [$this, 'async_action_download_thumbnail']);
    }

    public function wp_init()
    {
        add_filter('yotuwp_video_thumbnail', [$this, 'yotuwp_video_thumbnail'], 10, 2);
        add_action('wp_enqueue_scripts', [$this, 'wp_enqueue_scripts']);
        add_action('wp_print_styles', [$this, 'wp_print_styles'], 999);
    }

    /**
     * Remove Yotuwp javascript
     */
    public function wp_print_styles()
    {
        wp_dequeue_script('yotupro');
        wp_dequeue_script('yotu-script');
        wp_deregister_script('yotupro');
        wp_deregister_script('yotu-script');

        wp_enqueue_style(self::PLUGIN_NAME . '-css');
        wp_enqueue_script(self::PLUGIN_NAME . '-js');
    }

    public function wp_enqueue_scripts()
    {
        wp_register_style(self::PLUGIN_NAME . '-css', $this->plugin_url . 'style.css', [], self::PLUGIN_VERSION, true);
        wp_register_script(self::PLUGIN_NAME . '-js', $this->plugin_url . 'script.js', ['jquery'], self::PLUGIN_VERSION, true);
    }

    public function yotuwp_video_thumbnail($url, $video)
    {
        //error_log(__METHOD__ . ' url:' . $url.' video:'.var_export($video,true) );
        $videoId = $video->contentDetails->videoId;
        $thumb_filename = $videoId . '.jpg';
        //error_log(__METHOD__ . ' url:' . $url . ' videoId:' . $videoId);

        $thumbnail_url = null;
        if (file_exists($this->cache_path . '/' . $thumb_filename)) {
            $thumbnail_url .= $this->cache_url . '/' . $thumb_filename;
        } else {
            as_enqueue_async_action(self::ASYNC_ACTION_DOWNLOAD_THUMBNAIL, ['job_args' => [
                'videoId' => $videoId,
                'thumbnail_filename' =>  $this->cache_path . '/' . $thumb_filename,
                'thumbnail_url' => $url
            ]], self::PLUGIN_NAME);

            $thumbnail_url .= $this->plugin_url . '/img/loading-thumbnail.jpg';
        }
        //$url = 'https://wordpress.devhost/wp-content/tarteaucitron-lonely/youtube-yVaDWSEo7xo-maxresdefault.jpg';
        return $thumbnail_url;
    }

    public function async_action_download_thumbnail($args)
    {
        if (!is_dir($this->cache_path))
            mkdir($this->cache_path, 0777, true);

        $content = file_get_contents($args['thumbnail_url']);
        file_put_contents(
            $args['thumbnail_filename'],
            $content
        );
    }

    protected function ignoreRequest()
    {
        //error_log(__METHOD__ . ' ' . $_SERVER['REQUEST_URI']);

        if (defined('DOING_CRON') && DOING_CRON)
            return true;

        if (defined('DOING_AJAX') && DOING_AJAX && isset($_REQUEST['action'])) {
            switch ($_REQUEST['action']) {
                case 'heartbeat':
                case 'closed-postboxes':
                case 'health-check-site-status-result':
                case 'oembed-cache':
                default:
                    return true;
            }
            //error_log(__METHOD__ . ' ajax action:' . $_REQUEST['action']);
        }

        /*
        $uri = $_SERVER['REQUEST_URI'];
        if (strlen($uri) > 5 && strpos($uri, '.map', -5) !== false)
            return true;
        */

        return false;
    }
}
